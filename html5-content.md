# HTML 5
## Elementos basicos
```
<a>
<div>
<span>
<p>
<input>
<b>
<ul>
<ol>
<li>
<table>
<tr>
<td>
```
## Elementos Semânticos
```
<article>
<aside>
<details>
<figcaption>
<figure>
<footer>
<header>
<main>
<mark>
<nav>
<section>
<summary>
<time>
```
![](https://www.w3schools.com/html/img_sem_elements.gif)

## Shadow DOM

## Canvas
O elemento HTML `<canvas>` disponibiliza uma zona gráfica vazia, na qual é possível desenhar, através de APIs de javascript como canvas 2D ou WebGL.
```
<canvas id="myCanvas" width="200" height="100"></canvas>
```
### Desenhar linha
```javascript
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.moveTo(0,0);
ctx.lineTo(200,100);
ctx.stroke();
```

### Escrever texto com preenchimento
```javascript
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.font = "30px Arial";
ctx.fillText("Hello World",10,50);
```

### Escrever texto com contorno
```javascript
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.font = "30px Arial";
ctx.strokeText("Hello World",10,50);
```

### Desenhar quadrado com gradiente
```javascript
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");

// Create gradient
var grd = ctx.createLinearGradient(0,0,200,0);
grd.addColorStop(0,"red");
grd.addColorStop(1,"white");

// Fill with gradient
ctx.fillStyle = grd;
ctx.fillRect(10,10,150,80);
```

### Desenhar imagem
```javascript
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
var img = document.getElementById("scream");
ctx.drawImage(img,10,10);
```

## SVG
**Scalable Vector Graphics (SVG)** é uma linguagem baseada em XML markup, para desenhar vectores gráficos de duas dimensões.
### Vantagens de usar SVG
- Os gráficos SVG não perdem qualidade, mesmo quando é efectuado zoom ou redimensionado.
- Qualquer elemento no ficheiro SVG pode ser animado.
- SVG é uma recomendação W3C (World Wide Web Consortium)
```
<svg width="100" height="100">
  <circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" />
</svg>
```

## Video
O elemento HTML `<video>` serve para incluir videos em um documento.
```html
<video width="400" controls>
  <source src="mov_bbb.mp4" type="video/mp4">
  <source src="mov_bbb.ogg" type="video/ogg">
  Your browser does not support HTML5 video.
</video>
```

### Atributos
|Atributo|Tipo de valor|Descrição|
|---|---|---|
|autoplay|Boolean|Se especificado o vídeo reproduz automaticamente|
|controls|Boolean|Se especificado o browser adiciona os controlos de vídeo|
|loop|Boolean|Se especificado o vídeo reproduz novamente quando chega ao final|
|muted|Boolean|Se especificado o vídeo reproduz sem som|
|poster|String|Caminho (url) para uma imagem que é exibida enquanto o vídeo não é reproduzido|
|src|String|Caminho (url) para o vídeo a reproduzir|
|height|Number|Altura do vídeo em pixeis|
|width|Number|Largura do vídeo em pixeis|

### Tipos de Média
|Formato Ficheiros|Tipo de Média|
|---|---|
|Mp4|video/mp4|
|WebM|video/webm|
|Ogg|video/ogg|

### Tags de Vídeo
|Tag|Descrição|
|---|---|
|`<video>`|Define um vídeo|
|`<source>`|Define a fonte dos média (Aúdio/Vídeo/Imagem)|
|`<track>`|Elementos de texto que são visualizados durante a reprodução dos média (Legendas|Letras de músicas)|

## Audio
O elemento HTML `<audio>` serve para incluir audio em um documento.
```html
<audio controls>
  <source src="horse.ogg" type="audio/ogg">
  <source src="horse.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
```

### Atributos
|Atributo|Tipo de valor|Descrição|
|---|---|---|
|autoplay|Boolean|Se especificado o audio reproduz automaticamente|
|controls|Boolean|Se especificado o browser adiciona os controlos de audio|
|loop|Boolean|Se especificado o audio reproduz novamente quando chega ao final|
|muted|Boolean|Se especificado o audio é reproduzido sem som|
|src|String|Caminho (url) para o audio a reproduzir|
|volume|Double|Volume do audio a reproduzir|

### Tipos de Média
|Formato FIcheiros|Tipo de Média
|---|---|
|MP3|audio/mpeg|
|Ogg|audio/ogg|
|Wav|audio/wav|

## Geolocation
A interface Geolocation serve para obter programaticamente a geolocalização do dispositivo.

### Métodos
|Método|Descrição|
|---|---|
|getCurrentPosition()|Retorna a geolocalização do dispositivo|
|watchPosition()|Executa o callback definido sempre que a localização do dispositivo alterar|
|clearWatch()|Elimina/Para a execução do método watchPosition()|

### getCurrentPosition()
```javascript
var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};

function success(pos) {
  var crd = pos.coords;

  console.log('Your current position is:');
  console.log(`Latitude : ${crd.latitude}`);
  console.log(`Longitude: ${crd.longitude}`);
  console.log(`More or less ${crd.accuracy} meters.`);
};

function error(err) {
  console.warn(`ERROR(${err.code}): ${err.message}`);
};

navigator.geolocation.getCurrentPosition(success, error, options);
```

### watchPosition/clearWatch
```javascript
var id, target, option;

function success(pos) {
  var crd = pos.coords;

  if (target.latitude === crd.latitude && target.longitude === crd.longitude) {
    console.log('Congratulation, you reach the target');
    navigator.geolocation.clearWatch(id);
  }
};

function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
};

target = {
  latitude : 0,
  longitude: 0,
}

options = {
  enableHighAccuracy: false,
  timeout: 5000,
  maximumAge: 0
};

id = navigator.geolocation.watchPosition(success, error, options);
```

## Drag and Drop
A interface Drag and Drop do HTML5 possibilita as aplicações utilizarem as funções de drag and drop dos elementos HTML nas páginas dos browsers.

## Armazenamento
### Cookies

### Local Storage

### Session Storage

## ServiceWorker

## Web Worker