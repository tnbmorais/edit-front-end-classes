# CSS 3

## CSS Syntax e exemplos

## Formas de adicionar CSS
- Inline
- Interno
- Externo

## Cores
- nome
- Hex
- RGB

## Margins e paddings
- Diferenças

## Selectores
- Class
- Id
- Atributos

## Propriedades
- Dar exemplos de propriedades

## Listas
- Listas ordenadas
- Listas não ordenadas

## Positions
- Absolute
- Relative
- Static
- Fixed

## Float
- left
- right
- clear float

## Pseudo elementos
- Before
- After

## Propriedades default dos elementos html

## CSS Box Model
- Content
- Padding
- Border
- Margin

## Compatibilidade dos browsers

## CSS Reset CSS