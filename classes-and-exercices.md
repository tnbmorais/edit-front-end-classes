- git + npm
    - overview de como usar git
- html document and elements
    - desenhar páginas com utilização de todos os elementos.
- formulários
    - Desenvolvimento de formulário com backend. Exercicio com nomes moradas etc...
- html 5 diferences new features (shadow dom, semantic elements)
    - Migração de um documento html para html5.
- html5 (canvas e svg)
    - Utilização de ambos os elementos.
- html 5 media components (audio video)
    - Implementação de novos componentes multimédia.
- html 5 new api components (Storage / Geolocation / Workers)
    - Utilização de todas as apis.
- CSS principios
    - Estilizar uma página.
- responsive design
    - Media queries utilizar em uma página e adaptar para mobile.
    
    
    
http://www.pairuptocode.com/index.html
https://www.teaching-materials.org/csstools/exercises/exercise_bootstrap_descrip